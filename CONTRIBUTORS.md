| Contributor            | Email                    | Position      | 
| -----------------------| ------------------------ | ---------------
| Jorge Barrero          | @                        | Technology Business Advisor
| Juan Carlos Varela     | @                        | Computer Security Advisor
| Ariday Morales         | @                        | Consultant
| Leonardo Bolívar       | @                        | Consultant
| Yhoe Benitez           | @                        | Consultant Support
| Carlos Ruiz Lucchese   | carloslucchese@gmail.com | Project Manager
| Maxilmilano Bandres    | @                        | Consultant
| Magda Margarita  Girón | @                        | Academic Advisor
| Federico Infante       | @                        | Consultant
| Juan Andres Gonzalez   | @                        | Technology Business Advisor