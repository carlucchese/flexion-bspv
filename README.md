Buenas prácticas.

Principios Generales en una Fábrica de Software
[TOC]

## Antecedentes

Inevitablemente, los desarrollos de software enfrentan restricciones siempre. Algunas son impuestas por los recursos económicos disponibles, otros por los tiempos, las limitaciones técnicas, los entornos particulares, las tecnologías o los procesos involucrados.

El presente documento propone un conjunto de buenas prácticas que sin duda deben mejorar el ambiente de trabajo y la productividad en una Fábrica de Software.

## Concepto de una fábrica de software

Una fábrica de software es una empresa de la industria del software cuya misión es el desarrollo de software para sus clientes de acuerdo a los requisitos específicos que aquel le solicita.

Fuente:
https://es.wikipedia.org/wiki/F%C3%A1brica_de_software

Típicamente un fábrica de software tiene como su principal fuente de ingreso la venta de proyectos de desarrollo de software, como así también la venta de horas hombre de desarrollo de software. Generalmente la propiedad intelectual de las aplicaciones informáticas desarrolladas le pertenecen al cliente.
desde la toma de requerimientos y diseño funcional hasta la construcción, pruebas de aceptación e implantación; mientras que la segunda inicia su tarea a partir de un diseño funcional que "otros" han realizado. Por tanto, los skills de una factoría de software son claramente diferentes: requieren de un menor conocimiento de la necesidades del futuro usuario de negocio, existe más flexibilidad respecto a la ubicación geográfica de la factoría (al no requerir cercanía con el futuro usuario del sistema a fin de definir el requerimiento)y se especializan para conseguir calidad de software a menor coste, automatizando e industrializando los procesos de desarrollo de software donde aplique.

Por lo general la fábrica de software tiene ingresos adicionales por los servicios asociados que brinda a los clientes a los que les desarrolla las aplicaciones informáticas, tales como el mantenimiento, la capacitación, la actualización, el despliegue, el soporte, etc.

Existe una gran diferencia entre una fábrica de software de una fábrica de productos de software, dado que esta última se dedica al desarrollo de software para la generación de productos propios basados en aplicaciones informáticas cuya propiedad intelectual le pertenece.

Es necesario también tener en cuenta que al emprender una fábrica de software se debe capacitar muy bien al personal que va a trabajar para ser más eficientes y contribuyan al desarrollo continuo y avanzado en el transcurrir del tiempo y de las necesidades.

## El ciclo de vida de un proyecto en una fábrica de Software

El proceso de una fábrica de software se ejecuta de forma modular y en proyectos individuales que generan como producto final el código de un artefacto de software y los elementos complementarios, tales como la documentación y los resultados de las pruebas de calidad.
El ciclo de producción contiene las siguientes etapas:

### Venta del proyecto

* Exploración de necesidades del cliente
* Exploración del entorno tecnológico del cliente
* Estimación de la solución
* Propuesta económica
* Aprobación

### Gestión de requerimientos

* Conversión de necesidades a requerimientos
* Conversión de requerimientos a especificaciones técnicas

### Evaluación y gestión de riesgos

* Diseño del plan de gestión de riesgo

### Seleccion de tecnologias

* Documentar las tecnologías a utilizar
* Definición de los esquemas de licenciamiento utilizados
* Definición de esquema de licenciamiento generado
* Definición legal de uso de material propiedad del cliente
* Definición legal del uso de materiales de terceros

### Construcción del prototipo

* Propuesta visual
* Arquitectura tecnológica
* Construcción de datos de prueba
* Prototipo de alta fidelidad
* Plan de producción
* Aprobación del prototipo funcional

### Construcción de los artefactos

* Acuerdos de diseño y normas laborales
* Código de la funcionalidad
* Ajuste de funcionalidad a los estándares
* Pruebas unitarias
* Depuración y documentación
* Aprobación y entrega de artefactos

### Integración

* Acuerdos de integración
* Pruebas de integración
* Aprobación de la integración

### Pase a producción

* Acuerdos de pase a producción
* Deployment final

### Gestión de garantía

* Definición de términos de la garantía
* Resolución de casos por concepto de garantía

### Entrega y cierre de proyecto

* Entrenamiento
* Entrega documentación
* Finiquito técnico
* Finiquito administrativo

## Desarrollo del ciclo de vida

Para que un proyecto no genere sorpresas desagradables, hay que procurar controlar la mayor cantidad de elementos posibles desde el inicio del mismo o antes. En la vida real, los proyectos se parecen a lo que sucede en el mundo deportivo.

Una carrera de automóviles no se inicia cuando dan el banderazo de partida. La competencia comienza muchísimo antes.
Un dia antes, se asegura una buena posición de salida, dos días antes se practica sobre la pista donde se va a correr para familiarizarse con los detalles de última hora tales como huecos en la pista, condiciones climáticas, factores emocionales del piloto, entre otras.

El desarrollo de software no es diferente. El proyecto se entrega a tiempo y dentro de los parámetros de costo y calidad especificados, mucho antes de ser iniciado.

Entender el ciclo de vida de un proyecto de software es vital para el buen desarrollo. Y para ello hay que entender la naturaleza de la actividad a la que nos enfrentamos.

## Aspectos fundamentales

### Conociendo la actividad del desarrollo de software

El desarrollo de software es una actividad de gerencia de equipos de talento humano basados en principios de ingeniería.
Como proceso, se parece a un restaurante. También guarda muchas similitudes con los deportes que se realizan en equipo (que a nivel profesional son casi todos).
Un manager deportivo o un chef exitoso, podría trasladar muchas de sus técnicas al mundo de la producción del software y ser muy exitoso.

Ahora bien, el objetivo de un chef es generar comida de alta calidad y precio adecuado que justifique que un cliente coma en su restaurante y no en su casa.
Un manager deportivo tiene como objetivo ganar la mayoría de las confrontaciones y perder la minoría con los miembros del equipo que dispone.Un proyecto se concibe como un torneo de un deporte por equipos donde se gana en la medida en que todas las piezas de un rompecabezas calcen de forma impecable al final.
Para lograrlo hay que entrenar.

Se requieren conocimientos y habilidades. Esta actividad se parece mucho al alpinismo o al buceo. En sí misma no puede ser realizada por novatos ya que es extremadamente peligrosa. Solo si te gusta la adrenalina puedes vivir en este ambiente.
El desarrollador de software está expuesto a varios peligros inminentes tales como el aislamiento social, las enfermedades producidas por el estrés, las producidas por falta de actividad física, el cansancio mental, insomnio, las enfermedades visuales, la sensación de fracaso o síndrome del impostor entre otras.

Sin embargo, así como el alpinista sube la montaña simplemente porque está ahí, el desarrollador de software crea cosas simplemente porque puede hacerlo y otros no.
En desarrollador de software es un creador. Un artista en toda la extensión de la palabra. Sus obras son mágicas, sus logros extraordinarios.
Pero no todo el mundo sirve para cantante, por mucho que le guste la música. Para ser desarrollador hay que tener ciertas cualidades que lo hacen sobresalir del resto. Pero esto no es diferente para los atletas, los músicos o los actores.

### El talento no es suficiente. Aquí se requiere

* Un modelo de pensamiento donde la premisa es que todo es posible.
* Un actitud de persistencia inquebrantable.
* Una visión más allá de lo ordinario.
* Mucho conocimiento.
* Grandes habilidades relacionadas con los equipos de cómputo.

Un buen programador llegará a tener los mismos beneficios de una estrella de rock, un deportista famoso o un gran chef. Pero llegar ahí no es tarea sencilla.

### La labor del ingeniero

Aquí tenemos que estar claros que ser programador y ser ingeniero son cosas diferentes.

-IMG HERE-

La ingeniería es la intersección entre la ciencia y la economía. Su función es convertir los hallazgos científicos y convertirlos en actividades económicamente viables.
El programador por su parte es un artista que requiere supervisión, entrenamiento y guía. Alguien muy talentoso en lo que hace pero con visión escasa del mediano y largo plazo.

El ingeniero, se encarga fundamentalmente de controlar todas las condiciones que permitan hacer posible que se generen los proyectos de forma rentable dentro del los tiempos estipulados.
El norte es buscar un punto de equilibrio entre calidad de producto, efectividad de procesos y rentabilidad económica.
Al igual que en un partido de fútbol, dispone de un tiempo finito para lograr los objetivos, y en este caso es que se logren producir todos los artefactos previstos, con la calidad esperada dentro del presupuesto.
Ya que la materia prima de este proceso es el talento humano, debe conocer cómo aplicar técnicas que aprovechen al máximo el rendimiento de cada jugador.

**En una fábrica de software existen los siguientes roles de ingeniería**

* Vendedor de IT
* Gerente de proyecto
* Gerente de calidad

Pero un gran error conceptual que suele ocurrir es no entender que son roles complementarios dentro de una cadena de producción y no roles separados y opuestos.
La función que este equipo de ingenieros debe realizar es garantizar que los programadores generen la mayor cantidad de goles posibles en los 90 minutos de juego.
Estos ingenieros deben ver el partido desde afuera y crear las condiciones para que los jugadores hagan su tarea.
Bajo esta premisa existen muchas malas prácticas debido a la confusión de los roles.

El ingeniero nunca debe involucrarse en la producción de los artefactos, de la misma manera que el entrenador técnico no se mete en el campo de juego. Esa es labor de los programadores.
El ingeniero debe planificar las operaciones y ejecutarlas con sentido táctico, es decir, aprovechando los talentos a su disposición.

Su misión es lograr la mayor cantidad de artefactos en el menor tiempo posible, siguiendo las reglas establecidas y cumpliendo los estándares de calidad de producto.
Los ingenieros tienen cuotas, los programadores no. Los indicadores de gestión, son diferentes a los indicadores de desempeño y por lo tanto incompatibles.

Ahora bien, ventas, producción y calidad son los pilares de la estructura organizacional. Debe existir una figura de gerencia general que gobierne los tres.

Esta figura puede ser un persona o un comité. En caso de ser un comité, está conformado por los ingenieros responsables de ventas, producción y calidad. Aunque esto no suele ser buena idea pues requiere mucha madurez de los procesos, cosa que generalmente no existe.

Siguiendo la analogía del fútbol, podríamos tener 11 titulares, 6 suplentes, un gerente técnico, un dueño del equipo y un encargado de relaciones públicas.

### Tiempos y costos

A diferencia de otras actividades humanas, en el mundo de la producción de software los tiempos y los costos son entidades totalmente controlables y de maneras inimaginables.
La razón es que vivimos en un mundo virtual donde adicionalmente existe algo llamado mundos paralelos.
Esta característica nos permite de muchas maneras manipular el tiempo, pero así como puede jugar en pro, puede jugar en contra.
Durante el desarrollo del software, se viven diferentes momentos y se pueden organizar de infinidad de maneras. Una buena o mala decisión afectará todo el proyecto de manera irreversible.
Así que hay que cuidar aquellas decisiones que afecten la totalidad del proyecto en general y trabajar en pequeños bloques que si salen mal puedan rehacerse sin afectar el entorno.

Esto funciona como un grupo de fichas de dominó puestos una al lado del otra. Si decides poner 10.000 en un solo aliento, puedes ganar tiempo al principio, pero el riesgo de que uno se caiga y tumbe todo es muy alto.

Quizás una mejor estrategia es armar 10 grupos independientes de 1.000. Significa 10 veces más trabajo, pero también 10 veces menos riesgo.
Una mala decisión puede ser armar 1000 grupos de diez pues aunque se disminuye enormemente el riesgo, también se incrementa enormemente el trabajo.

Este es el juego del balance que debe considerar un ingeniero de software.
Por otro lado debemos considerar los niveles de dificultad. En general, es mejor idea hacer lo facil primero y dejar lo difícil para el final. Eso siempre y cuando se puedan diferenciar.
Pero en esta actividad también hay un factor al que se le da poca importancia que es el nivel de ignorancia.
Si no se sabe realmente cómo solucionar algún artefacto, no se debe confundir el concepto de " es difícil" con el concepto de "no se como se hace".

Las cosas que se ignoran hay que abordarlas en las fases tempranas del proyecto y manejarlas por separado pues generalmente hay mucha incertidumbre en cuanto a tiempos y costos para estas actividades. 
Y aunque lo ideal es no tenerlas, parte de los retos del software es que estos factores existen así como existen tiburones para el buzo o aludes para el alpinista.

Cuando conocemos la forma de resolver un artefacto simplemente lo construimos siguiendo una línea de pensamiento y un procedimiento estandarizado. Esta metodología se llama top-down.
Por el contrario si no conocemos la forma de resolver el asunto, usamos una metodología bottom-up.
Sin entrar en muchos detalles podemos decir que en la primera se pueden predecir los tiempos y los costos con bastante precisión, en la segunda no.

La idea general es que en un proyecto existan muchos top-down y pocos bottom-up. Una actividad bottom-up siempre dará resultados extraordinarios, al igual que su costo.
Otro aspecto importante es el seguimiento diario y la metodología para hacer los ajustes en las entregas según los resultados obtenidos.

Para lograr un efectivo seguimiento diario, los programadores deben hacer entregas diarias de productos completos, sin posibilidad o con muy baja posibilidad que una actividad quede pendiente para el próximo día.
Esto se logra con la pŕactica, así como se logra con la práctica que un tenista gane un set.
Así como existe un tamaño máximo de longitud de archivo para una funcionalidad, también existe un tiempo máximo para entregar un artefacto. Y el tiempo máximo es un día y la longitud máxima es de archivo es de 400 líneas.
Las actividades se deben categorizar en 2, 4 y 8 horas. Al final del día, cada programador entrega 1,2 o 4 actividades concluidas.
Una actividad concluida es aquella que realiza la funcionalidad y el código está ajustado a los acuerdos de diseño.
Es similar a ganar un set de tenis o meter un gol. Ganar un punto o meter un gol no garantiza que se gane el torneo, pero definitivamente es la vía.

Todo artefacto computacional se produce de la misma manera que se produce la comida en una cocina y lleva exactamente las mismas etapas y características. Si tiene dudas sobre su proceso de software, un buen ejercicio es ver cómo funciona la cocina de un restaurante.

### Ejemplo de la ejecución de una actividad

Como ejemplo, digamos que se tiene que construir un sistema de autenticación (login/logout).
Para tener una idea de esta actividad, debemos aclarar que esta jamás sería la primera actividad como podría imaginarse. 
Llegar a este punto ha implicado entre otras cosas tener una negociación con el cliente, haber acordado la existencia de esta funcionalidad, haber realizado un prototipo donde la funcionalidad está incluida y los datos de prueba existen, tener ya configurado un ambiente de producción, y un ambiente de calidad y un ambiente de desarrollo, deben existir acuerdos sobre las normas de diseño que incluyen el lenguaje de computación a utilizar, los formatos de datos de entrada y los formatos de datos de salida y haber elaborado un plan de producción para determinar la metodología top-down o bottom-up a ser utilizada.

Pretender abordar una situación de estas sin estos condicionantes es tan absurdo como pretender ganar una medalla olímpica de nado sincronizado sin haber asegurado que existe al menos una piscina y que los atletas saben nadar.
Asumiendo que existen estas condiciones, el programador o equipo de programación (en caso de usar metodologías tipo xp), genera el artefacto en su primera aproximación y lo presenta al gerente de producción para obtener la aprobación de la funcionalidad.
En esta fase, hay libertad creativa y el único objetivo es que el artefacto cumpla el conjunto de requerimientos exigidos. El artefacto debe permitir un registro del usuario un logueo deslogueo y una recuperación de contraseña. Si hace esas tres cosas, la prueba está superada.

El paso dos es adaptar el código a los acuerdos de diseño, que implica una refactorización. El código debe tener las variables, número de archivos, acuerdos de uso de variables y demás elementos de forma cubiertos y es remitido a la unidad de calidad.
Aquí tenemos otro error común al pensar que calidad es el enemigo.
Hasta el momento, los programadores hacen el trabajo del arquero y de la defensa en un partido de fútbol. Calidad son los delanteros, los que meten el gol.
A menos que exista un peligro inminente o una razón estratégica, calidad no devuelve la pelota.
Calidad toma el codigo, y lo perfecciona a tal punto que no solo cumple con el estándar, sino que lo supera. Esto se logra porque se prueban las funcionalidades en condiciones extremas, se documenta el código y se genera la documentación para el usuario tanto técnico como funcional.

Calidad es lo que es el emplatado en una cocina gourmet. Calidad es quien pone la guinda a la torta, es el empaque del artefacto fabricado por producción.

La gente de calidad son programadores igual que los de producción, sólo que juegan el rol de delanteros. Es muy probable, que su trabajo tenga más visibilidad que la del resto del equipo.
Una vez decorada y probada, la funcionalidad se integra al ambiente de producción siguiendo los protocolos y realizando los ajustes finales.

Si por alguna razón la integración de la funcionalidad no es exitosa, se requiere un análisis de postproducción y probablemente tenga el mismo impacto que un penalti a los 89 minutos del juego.
Sin embargo, perder un partido no es perder la oportunidad de ganar, simplemente es algo que hay que abordar con criterio y sensatez.
Si por perder un partido decidimos botar a los jugadores o penalizarlos de maneras crueles o injustas, lo único que logramos es desmoralizar al equipo y reducir las probabilidades de éxito.
Si por el contrario, capitalizamos nuestras derrotas, aprendemos de ellas, establecemos estrategias para sortear estas situaciones, definitivamente nuestro equipo será imparable.
Se debe notar la diferencia.

Si el equipo trabaja mucho al inicio, al final debe estar relajado y tranquilo, sin descuidarse en en lo obvio. Digamos como si estuviéramos ganando cinco a una a la mitad del segundo tiempo.
No descuidarse en este punto es esencial, pero desgastar a la gente en forma inútil solo nos da desventajas en el siguiente torneo.
Aquí hay que hacer énfasis en el asunto de los esquemas de compensación. Los buenos ingenieros se mueven por el dinero igual que los buenos productores de cine.

Sin embargo, los buenos programadores al igual que los buenos artistas o los buenos deportistas, si bien es cierto que los puedes mover inicialmente con el dinero, hay otras cosas que valoran más. Un buen ingeniero y un buen productor de cine no solo lo saben sino que lo usan en su favor.

## Roles dentro de una fábrica de software

Independientemente de la metodología que se quiera aplicar, existen roles estructurales dentro de la Fábrica de Software que definiremos aquí.

### Vendedor de IT
  
El vendedor de una fábrica de Software es un skill altamente profesional, generalmente con una experiencia mínima de 10 años en el mercado tanto del hardware o del software. Generalmente ingeniero de sistemas, telecomunicaciones, computación o cualquier rama afín tal como desarrollador de aplicaciones, mercadólogo de áreas tecnológicas.

Sus funciones abarcan todo el ciclo del desarrollo del proyecto que va desde la prospección de los clientes, primeras visitas de contacto, levantamiento de necesidades, levantamientos de requerimientos, elaboración de las propuestas comerciales, interfase con el cliente en las labores administrativas tales como facturación, cobranza y seguimiento, relaciones públicas con el cliente, gestión de cambios de alcance del proyecto en caliente, seguimiento de entregas parciales, entregas finales y finiquitos administrativos, revisión de acuerdos formales, legales, contractuales así como los aspectos económicos y financieros.

Es fundamentalmente un generalista que debe poseer amplio dominio tanto de los procesos internos de la Fábrica como de los procesos asociados a las necesidades de los clientes.
Debe ser un gran conocedor de las tecnologías vigentes así como de las obsoletas, tener una visión futurista para poder proponer soluciones innovadoras y al mismo tiempo una visión realista que sea capaz de aterrizar propuestas de desarrollo viables tanto en costo como en tiempo.

Generalmente estamos hablando de un profesional que tiene que compartir su tiempo entre largos periodos de calle con largos periodos de oficina y muchos altibajos en cuanto a los picos de intensidad del trabajo.
Requiere mucha capacidad de negociación pues debe intervenir en los procesos de estimaciones y negociaciones de precio, alcances y cambios de alcance, tiempos de entrega, condiciones de trabajo.
El vendedor de IT, al contrario de lo que suele creerse, participa en el proceso de producción desde antes de la aprobación y finaliza posterior a la entrega. Su rol durante la ejecución del trabajo se transforma, al convertirse en el mediador entre el cliente y el equipo de producción en momentos estelares tales como el kickoff, el cierre y las negociaciones de cambio de alcance.

El indicador por excelencia que mide la gestión de un vendedor de IT es la cantidad de horas vendidas. Otros indicadores complementarios son: nuevos clientes, visitas realizadas, efectividad de la visita, activación de clientes y clientes recurrentes.

### El coordinador de proyecto
  
Este rol lo realiza un ingeniero con altas capacidades de liderazgo tanto por sus conocimientos de los temas tecnológicos como de los procesos tanto internos como externos.
Es un profesional con muchas competencias blandas donde sobresalen el trabajo en equipo, la planificación, seguimiento y supervisión. Es conocedor de la psicología humana e interactúa con las personas con facilidad.

El coordinador tiene un liderazgo natural en el equipo, es admirado y respetado por sus compañeros y los apoya en momentos difíciles.
Su mayor competencia es conocer y anticiparse a las particularidades de cada miembro de su equipo, pudiendo aprovechar a su favor todas las circunstancias que hacen posible la alta productividad y desempeño.

El coordinador de proyecto interactúa con el personal técnico y funcional del cliente y lo mantiene informado de los avances, recibe las quejas en el procesos de desarrollo y las procesa, asigna las tareas durante los sprint de trabajo y lidera las reuniones diarias.
Todos su productos son entregados a la unidad de calidad para su refactorización y depuración.

### El gerente de Calidad
  
Es un ingeniero con altos estándares, visión de detalle, pensamiento estructurado y habilidad documental.
Su función es mejorar el código antes de pasarlo a producción por lo que debe contar con su equipo de programadores para lograr la tarea.
A diferencia del coordinador de proyecto y el vendedor de IT, el gerente de Calidad si interviene directamente en el código generado y produce materiales, aunque disponga de un equipo de gente para realizar su trabajo.

El gerente de calidad es un catador.
A este punto podemos decir que la disciplina de calidad en código es un entrenamiento y una actitud frente al código.

Las personas que realizan esta tarea exitosamente son muy detallistas y de mentalidad estructurada, amantes del buen gusto y de las cosas bien hechas. El objetivo de la unidad de calidad es convertir lo excelente en extraordinario.

Los programadores de producción generan código brillante que resuelve problemas de manera ingeniosa. Sin embargo la apariencia, empaquetado, consistencia formal estructura, documentación y otros detalles, son agregados y mejorados por  la unidad de calidad de forma tal que se reduzca la deuda técnica y se logre la mantenibilidad del código en el tiempo.

La premisa es que si la gente de calidad pudo entender cómo se aplicó la solución, y además comenta el código de manera estandarizada e incluye la documentación para los usuarios finales, tanto técnicos como funcionales, las posibilidades de poder refactorizar y reutilizar este código en el futuro son realmente buenas.

### El programador
  
Tomemos por un momento la industria de la música como referencia y veamos algunos realities:

* American Idol
* Talento Fox
* Factor X
* Operación Triunfo

Y veamos otros similares pero en el ámbito de la cocina:

* Masterchef
* Chopped
* Buddy

Y miles que existen… que tenemos en común:
Son aficionados aprendendiendo a ser profesionales en actividades donde el talento es la clave.
La pregunta es…

¿Se puede tomar a alguien totalmente ignorante de un tema y convertirla en una persona competente para una actividad de esta naturaleza?

La respuesta es definitivamente si.

Estos programas han demostrado en incontables ocasiones que gente común y corriente, con ciertas características particulares y la guía adecuada pueden pasar de nivel cero absoluto a personas altamente competentes.

¿Cómo se aplica esto al mundo de los programadores?

La primera idea a transmitir es que programar es una actividad donde privan las habilidades más que los conocimientos, igual que cantar.
Lo segundo es que el conocimiento no es suficiente, tampoco lo es la habilidad.

Aunque cada uno tenga unos gustos particulares, un buen chef o un buen cantante se "especializan" después de haber paseado por muchas técnicas y estilos. Sólo cuando has probado hacerlo todo, puedes decidir hacer un nicho de mercado en un segmento especial y no al revés.

Se requiere mucho trabajo, dedicación y esfuerzo si se quiere llegar lejos.

El proceso de aprendizaje es una actividad contínua y permanente.

## Los programadores que tenemos y buscamos

Cuando nos llega un proyecto, buscamos gente por sus conocimientos específicos en algún lenguaje o tecnología, más que por sus cualidades y talentos. Generalmente porque los presupuestos son apretados y creemos que el programador que nos va a resolver el problema es el que domina un asunto de manera muy específica.
En general esto es un error de ingeniería en el diseño ya que el 80% del código necesario para producir un artefacto computacional está ya hecho y nuestra tarea es copiarlo, adaptarlo y pegarlo de alguna manera. En algunas ocasiones es menor aún el porcentaje.

Digamos que quieres hacer un sitio de Wordpress. El código de wordpress ya está hecho y si divides aquello que agregas (sin contar el contenido que es asunto de usuario) y lo divides por el tamaño total de la aplicación verás a lo que nos referimos.

Ahora bien, ¿Qué hace el programador?

Construye lo que falta, completa el rompecabezas. En la industria moderna ya nadie hace las cosas desde cero, y aunque las haga en la realidad usa muchas herramientas que han sido construidas previamente por otras personas.

Así que nada de creer que el programador es el centro del universo, es simplemente una pieza más del rompecabezas.

Igual que un gran cantante, para ser exitoso requiere de todo un equipo para poder hacer su trabajo. Concretamente, requiere de alguien que comercialice, alguien que dirija el proyecto y alguien que revise el resultado. Si esas figuras, el programador no tiene más relevancia que alguien que canta en el baño.

Programar aplicaciones es un trabajo de equipo.

### Características de un buen programador

Decir que un buen programador es el que programa es como decir que un buen cocinero es el que cocina o el que canta.
Se necesita muchísimo más que simplemente conocer un par de lenguajes. Es más, conocer un lenguaje es equivalente a saberse un par de recetas o conocer un par de canciones.
El lenguaje es ciertamente importante, pero acompañado de muchas otras cosas más.

Si un programador solo sabe un lenguaje es como el chef que solo sabe preparar un plato. Puede ser muy exitoso, pero en general nunca va a pasar de hacer perros calientes en un sitio callejero.

Puedes ser exitoso también cantando al saberse unas canciones y usar unas pistas digitales. Pero probablemente no llegará más allá de las bodas los fines de semana.
Pero a los programadores se les han subido los humos dada la gran demanda en el mundo y para una organización dedicada al desarrollo, resulta increíblemente complejo estimar tiempos y costos basado en gente con tantos vicios e indisciplinas.

Es hora de poner orden en esta industria.

#### Comunicarse

La primera habilidad de un programador es la de poder comunicarse. Esto incluye comunicarse en clave morse, con señas, con el teclado, con señales de humo con protocolos TCP, en binario, en hexadecimal, en lo que sea.
Una buena prueba para saber si tienes estas características es la de poder jugar mímica, o la variante donde en lugar de gestos se usan dibujos. 
No te convierte en buen programador, pero definitivamente ayuda.
En tiempos inmemoriales, para ser cantante solo se necesitaba cantar bonito. Hoy no es suficiente. Para ser buen cocinero se requería cocinar rico, hoy en día no es suficiente y la razón es que ya no trabajamos solos, trabajamos en equipo donde la comunicación es fundamental.

***La programación es el arte de comunicar gente con sistemas de cómputo.***

#### Investigar

La realidad es que los problemas con los que tenemos que lidiar hoy en día requieren un serio sentido de la investigación. Un programador no puede simplemente aplicar las recetas conocidas a todos los casos de la vida. Y en la realidad, esta industria cambia de forma tan vertiginosamente rápida que es imposible afirmar que la solución que conseguimos hoy será la mejor mañana.
An problemas muy pequeños requieren hacer una pausa e investigar algunas cosas. Pero debido a que los programadores han concentrado su atención en formarse en ciertas técnicas o lenguajes, generalmente carecen de las habilidades necesarias para investigar de forma sistemática y eficiente sobre un tema en particular.

Normalmente el programador se excusa y se esconde ante lo desconocido ya que prefiere la comodidad de su lenguaje o su sistema operativo favorito.
Este es un vicio que hay que erradicar de entrada.
En nuestro caso, el programador investiga para aplicar aquello que encuentra como paradigma o herramienta para solucionar un problema.
Google es la base fundamental de la investigación de un programador. Así que como parte de sus competencias, debe saber buscar en Google, así como en otros sistemas de búsqueda.
Las habilidades de búsqueda deben ser muy elaboradas incluyendo, buscar en otros idiomas desconocidos, interpretar y sintetizar la información extraer elementos relevantes y tener la habilidad de separar las agujas en el pajar.

***La programación es el arte de traducir el conocimiento de uno a otros.***

#### Crear código

Un programador crea código en cualquier lenguaje. Y debe poder hacerlo.
Uno de los mitos más grandes es creer que solo hay que concentrarse en algunos pocos lenguajes o paradigmas de programación.

Un programador debe poder abordar las tareas de aprender un nuevo lenguaje, aprender sus reglas y crear código útil y adaptado a los estándares.
Una cosa es diseñar una solución computacional y otra es convertir esa solución en código ejecutable.

Un programador es bueno en lo primero y no necesariamente en lo segundo, pero lo aprende con facilidad.
Algo de práctica y el programador tendrá una nueva habilidad y un lenguaje nuevo en su arsenal.

***La programación es el arte de convertir ideas en código de computadora.***

#### Disciplina

Toda actividad humana hecha a escala industrial requiere disciplina. Esta se traduce en el mundo computacional en tres cosas simples: orden, limpieza y consistencia.

##### Orden

Entendemos el orden por aquella capacidad de comprender que cada cosa tiene un lugar, que hay un lugar para cada cosa y que en general el orden es 100% arbitrario.
No es lo mismo arreglar tu cuarto funcionalmente que en orden alfabético. Descendente y ascendente son implicaciones de orden pero opuestos en naturaleza.
Ser ordenado en el caso de un programador es una interpretación algo diferente ya que él encuentra orden y lo fabrica.

El orden tiene una implicación importante que es la secuencialidad y el paralelismo de las cosas. Así que el programador entiende que se hace antes y que se hace después, al igual que entiende que se puede hacer en simultáneo.

##### Limpieza

En el mundo computacional, limpieza significa deshacerse de lo que no sirve. El escritorio debe estar limpio, los proyectos deben estar limpios, el código debe estar limpio y eso al final significa que todo debe tener solo lo que es útil.

##### Consistencia

La consistencia es la habilidad de usar los mismos elementos sin variaciones para garantizar su reutilizabilidad.
En el mundo computacional la consistencia es vital. Ser consistente ayuda al programador a pensar en lo importante sin ocuparse de las excepciones, así que él aprende desde muy temprano en su carrera que al establecer unas reglas estas deben mantenerse a toda costa dentro del contexto elegido.

La consistencia aplica de forma diferente en los eventos secuenciales y los eventos en paralelo, así que el programador, debe entender y manejar el concepto de consistencia eventual.
Para ilustrar esta idea, veamos una canción. Si el cantante la decide interpretar en Do mayor, debe ser consistente durante toda la canción. Si decide cantarla en Sol, o en Re, la canción debe ser consistente de principio a fin.
Eso no quiere decir que todas las canciones se cantan siempre igual y en el mismo tono. Lo que implica es que una vez seleccionado, este no debe cambiar para esa canción en particular.

***La programación es una disciplina que involucra arte, destreza y conocimiento por igual.***

#### Matemáticas

Nada más cierto que el programador de computadoras debe ser un matemático ante todo. Y en este aspecto queremos enfatizar que el programador no es un ingeniero y por ende no usa las mismas áreas de las matemáticas en las mismas proporciones.
El método científico es uno, pero el método matemático es otro, similar pero no exactamente igual.
Para ser programador se requieren ciertas habilidades matemáticas muy concretas y que describimos a continuación:

##### Contar

Parece fácil pero no lo es.
Contar es una habilidad fundamental porque permite tener una noción de las cantidades, los ciclos, el número de veces y muchas veces el orden imperceptible de situaciones muy complejas.
Un ejemplo simple el la capacidad para contar un ciclo dentro de otro ciclo. Si alguien te dice cuenta cuántas cosas tienes en tu escritorio y forma grupos por color, es un caso simple donde hay que saber contar y donde la mayoría de las personas fallan.
El programador se enfrenta a situaciones mucho más complejas de conteo incluyendo casos de borde, cambios de estado y datos mutables.

##### Sumar y multiplicar

La realidad es que esto es lo que realmente hay que saber hacer. Todos los demás cálculos generalmente los delegamos a las capacidades de cálculo de las computadoras que son superiores.

##### Geometría

No se puede ser un buen programador si no se tiene una noción geométrica del mundo que nos rodea ya que al final el programador crea un mundo virtual donde habitan conceptualmente los elementos de la realidad conocida.
Sin importar que tipo de aplicaciones se hagan, el programador debe conocer las formas físicas, y sus interrelaciones euclidianas y no euclidianas.

##### Algebra lineal

##### Grafos

##### Algebra relacional

##### Matemática discreta

##### Teoría de números

#### Lingüística

## Modelo de Gestión

### Gestión de riesgo e incertidumbre

Cada actividad debe reflejar numéricamente el riesgo y la incertidumbre asociada. Si no se puede determinar de forma razonable, la actividad hay que atomizarla y colocarla como condicionante de otras actividades.